import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import './common.css'

import {Employee} from './employee.js'
import {Register} from './register.js'

const title = <h1>社員情報</h1>

class MainClass extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLogedIn: false
    }
  }

  render() {

    let logInButton;
    if(!this.state.isLogedIn) {
      logInButton = <button className="logInButton" onClick={() => logInButtonClick()}>ログイン</button>
    } else {
      logInButton = <button className="logInButton" onClick={() => logInButtonClick()}>ログアウト</button>
    }

    const logInButtonClick = () => {
      this.setState({isLogedIn: !this.state.isLogedIn})
    }

    // 社員一覧を作成
    const employees = [
      {id: 4, name: "佐藤", department: "総務人事部"},
      {id: 2, name: "鈴木", department: "営業部"},
      {id: 3, name: "田中", department: "営業部"},
      {id: 1, name: "テスト", department: "テスト"}
    ]

    // propsを設定したEmployeeコンポーネントを使って一覧表を作成
    const employeeList = employees.map((employee) => (
      <Employee key={employee.id} employee={employee.name} department={employee.department}/>
    ))

    return(
      <div>
        {title}
        {this.state.isLogedIn &&
          <div>
            <table className="emloyeeList">
              <thead>
                <tr><th>名称</th><th>部署</th><th>稼働状況</th><th>稼働切替</th></tr>
              </thead>
              <tbody>
                {employeeList}
              </tbody>
            </table>
            <br/>
            <Register />
          </div>
        }
      {logInButton}
      </div>
    )
  }
}

MainClass.propTypes = {
  employee: PropTypes.string
}

ReactDOM.render(
    <MainClass />,
    document.getElementById('root')
)