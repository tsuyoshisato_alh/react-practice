import React, {Component} from 'react'
import PropTypes from 'prop-types'
import './common.css'

export class Employee extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isWorkingFlg: true,
    }
  }

  render() {
    const handleClick = () => {
      this.setState({isWorkingFlg: !this.state.isWorkingFlg})
    }

    return(
      <tr>
        <td>{this.props.employee}</td>
        <td>{this.props.department}</td>
        <td>{this.state.isWorkingFlg ? "稼働中" : "停止中"} </td>
        <td><button onClick={handleClick}>{this.state.isWorkingFlg ? "停止" : "稼働"} {this.state.count}</button></td>
      </tr>
    )
  }
}

Employee.propTypes = {
  employee: PropTypes.string,
  department: PropTypes.string
}