import React, {Component} from 'react'
import './common.css'

export class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: ''
    }
  }

  render() {
    const handleChange = (e) => {
      this.setState({name: e.target.value})
    }

    const handleClick = () => {
      console.log(this.state.name)
    }

    return(
      <div>
        <label>名称</label>
        <input type="text" onChange={handleChange}/>
        <button onClick={handleClick}>出力</button>
      </div>
    )
  }
}